#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define DAYS_IN_WEEK    7
#define MONTHS_IN_YEAR  12

typedef struct {
    size_t day;
    size_t month;
    size_t year;
} Date;

const char *days[] = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"};
const char *months[] = {"January", "February", "March", "April", "May", "June", "July", 
                        "August", "September", "October", "November", "December"}; //uuffff!!
const size_t days_in_month[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

void print_month(size_t month, size_t year);
size_t get_day_of_week(size_t day, size_t month, size_t year);
size_t get_day_in_month(size_t month, size_t year);
int is_leap_year(int year);

void get_current_date(Date *date);
void print_calendar(const Date *date);
void print_usage(const char *prog_name);


int main(int argc, char *argv[]) 
{
    Date current_date;
    get_current_date(&current_date);
    
    if (argc == 1) {
        print_month(current_date.month, current_date.year);
    } else if (argc == 2) {
        size_t year = atoi(argv[1]);
        for(size_t m = 0; m < MONTHS_IN_YEAR; m++) {
            print_month(m, year);
            printf("\n");
        }
    } else if (argc == 3) {
        size_t month = atoi(argv[1]) - 1; //index stuff
        size_t year = atoi(argv[2]);
        print_month(month, year);
    } else {
        print_usage(argv[0]);
        return 1;
    }
    return 0;
}

void usage(const char *program_name) 
{
    fprintf(stderr, "Usage: %s[month year] | [year]\n", program_name);
}

int leap_year(int year) 
{
    return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0); // totally didnt yank this from gpt
}

int get_days_in_month(size_t month, size_t year);
